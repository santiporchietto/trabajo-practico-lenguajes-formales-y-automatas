
"""Primer Punto"""

def clasificar_gramatica(gramatica):

    #Separamos las reglas que componen a la gramatica
    reglas = gramatica.split("/n")

    #Creamos tres listas que almacenaran las reglas que no se cumplen de cada gramatica
    lg1 = []
    lg2 = []
    lg3 = []

    #Por cada regla de la gramatica determinamos el tipo al que pertenece
    for regla in reglas:
        #Separamos el antecedente del consecuente
        antecedenteConsecuente = regla.split(":")

        #Separamos los caracteres que componen tanto al antecedente como al consecuente
        antecedente = antecedenteConsecuente[0].split(" ")
        consecuente = antecedenteConsecuente[1].split(" ")

        #Inicializamos booleanos en falso que seran seteados en true cuando la regla no pertenezca a la gramatica
        g3 = False
        g2 = False

        #Si el antecedente tiene mas de un caracter o es un terminal, entonces la regla no pertenece a G3 ni G2
        if len(antecedente) > 1 or determinarSimbolo(antecedente[0]) ==  "t":
            g3 = True
            lg2.append((regla, "La regla tiene mas de un caracter en el antecedente, o este es un terminal"))
            g2 = True

        #Si la regla puede ser G3
        if not g3:
            #Si el consecuente tiene mas de dos caracteres no es G3
            if len(consecuente) > 2:
                g3 = True
            #Si el consecuente tiene dos caracteres verifica que sea terminal seguido de no terminal o viceversa
            elif len(consecuente) == 2:
                #Si la combinacion no es correspondiente a G3 entonces seteo el booleano
                if not ((consecuente[0] == consecuente[0].lower() and consecuente[1] == consecuente[1].upper()) or
                         (consecuente[0] == consecuente[0].upper() and consecuente[1] == consecuente[1].lower())):
                    g3 = True
            #Si el consecuente tiene un simbolo este debe ser terminal o lambda para ser G3
            elif determinarSimbolo(consecuente) == "NT":
                g3 = True

        #Si la regla no pertenece a G3 se agrega a la lista con la que formaremos el diccionario
        if g3:
            lg3.append((regla, "No pertenece a ninguna de las formas NT -> t, NT -> NT t, NT -> t NT, NT -> lambda"))

        #Si la regla deriva en lambda verifico si el antecedente sea el distinguido y no presente recursion
        if determinarSimbolo(consecuente[0]) == "L":
            #Verifico si es el distinguido
            if g2 or reglas[0].split(":")[0] != antecedenteConsecuente[0]:
                #No se trata del distinguido porque hay mas de un caracter a la izquierda o no coincide el antecedente
                lg1.append((regla, "La regla deriva en lambda y no corresponde a la regla del distinguido"))
            elif encontrarRecursividadDistinguido(reglas):
                #Hay recursividad en el distinguido
                lg1.append((regla, "El distinguido deriva en lambda y presenta recursividad"))
        elif len(antecedente) > len(consecuente):
            lg1.append((regla, "El antecedente tiene mas caracteres que el consecuente"))
        elif not verificarNoTerminales(antecedente):
            lg1.append((regla, "El antecedente no tiene no terminales"))

    diccionario = {'3' : lg3, '2' : lg2, '1' : lg1, '0' : []}
    return diccionario

#Busca recursividad en la regla del distinguido
def encontrarRecursividadDistinguido(reglas):
    antecedenteDistinguido = reglas[0].split(":")[0]
    for regla in reglas:
        # Separamos el antecedente del consecuente
        antecedenteConsecuente = regla.split(":")

        #Si el antecedente del distinguido es igual al antecedente de la regla actual
        if antecedenteDistinguido == antecedenteConsecuente[0]:
            # Separamos los caracteres que componen al consecuente
            consecuente = antecedenteConsecuente[1].split(" ")

            # Si el antecedente esta en el consecuente indica que hay recursividad
            if antecedenteDistinguido in consecuente:
                return True
    return False


#Funcion que determina que tipo de caracter es un simbolo pasado como parametro (terminal, no terminal o lambda)
def determinarSimbolo(simbolo):
    if simbolo == "lambda":
        return "L"
    elif simbolo[0] == simbolo[0].lower():
        return "t"
    else:
        return "NT"

#Funcion que retorna si hay no terminales en el antecedente
def verificarNoTerminales(antecedente):
    for simbolo in antecedente:
        if simbolo[0] == simbolo[0].upper():
            return True
    return False



"""Segundo Punto"""

class AutomataPila:
    """ Esta clase implementa un automáta de pila a partir de la definición de
    estados y transiciones que lo componen, pudiendo validar si una cadena dada
    puede ser reconocida por el mismo.
    """

    def __init__(self, estados, estados_aceptacion):
        """ Constructor de la clase.

        Args
        ----
        estados: dict
            Diccionario de estados que especifica en las claves los nombres de los
            estados y como valores una lista de transiciones salientes de dicho estado.
            Cada transición se compone de: (s,p,a,e) siendo
            s -> símbolo que se consume de la entrada para aplicar la transición.
            p -> símbolo que se consume del tope de la pila para aplicar la transición.
            a -> lista de símbolo/s que se apila una vez aplicada la transición.
            e -> estado de destino.

            Ejemplo:
            {'a': [('(', 'Z0', ['Z0', '('], 'a'),
                   ('(', '(', ['(', '('], 'a'),
                   (')', '(', [''], 'b')],
             'b': [(')', '(', [''], 'b'),
                   ('$', 'Z0', [''], 'b')]}

        estados_aceptacion: array-like
            Estados que admiten fin de cadena.

            Ejemplo:
            ['b']
        """

        self.estados = estados
        self.estados_aceptacion = estados_aceptacion
        self.estado_actual = None
        self.pila = ["Z0"]

    def validar_cadena(self, cadena):
        """ Se valida si una determinada cadena puede ser reconocida por el autómata.

        Args
        ----
        cadena: string
            Cadena de entrada a reconocer.

        Returns
        -------
        resultado: bool
            Indica si la cadena pudo se reconocida o no.
        """

        self.estado_actual = list(self.estados.keys())[0]

        for c in cadena:
            # Separo las transiciones posibles del estado actual
            transiciones = self.estados.get(self.estado_actual)

            # Extraigo el tope de la pila
            tope_pila = self.pila.pop()
            caracter = False

            for transicion in transiciones:
                # Verifico si hay una transicion que espere c en la entrada y consuma el tope de pila o no consuma nada
                if (transicion[0] == c) and (transicion[1] == tope_pila or transicion[1] == ''):
                    # El caracter de entrada de la cadena es valido
                    caracter = True

                    # Seteo el nuevo estado actual, que es el guardado en la transaccion actual
                    self.estado_actual = transicion[3]

                    # Agrego a la pila los caracteres correspondientes cuando este es distinto de lambda
                    if transicion[2][0] != '':
                        # Si de la pila no se lee nada
                        if transicion[1] == '':
                            # Agrego el tope de pila que saque anteriormente
                            self.pila.append(tope_pila)
                        # Agrego los demas caracteres a la pila
                        self.pila.extend(transicion[2][::-1])
                        break

            # Si el caracter no forma parte de los caracteres esperados en las transacciones de ese estado retorna False
            if not caracter:
                return False
        # Si el estado actual esta en los estados de aceptacion y la pila esta vacia entonces devuelve True
        return self.estado_actual in self.estados_aceptacion and self.pila == []




"""Pruebas primer punto"""
"""
a = clasificar_gramatica("A:b A/nA:a/nA:A B c/nA:lambda/nB:b")
b = clasificar_gramatica("S:a B/nS:c/nC:c A/nB:b C/nB:b/nA:a A/nA:a")
c = clasificar_gramatica("S:C b a/nS:C/nC:B c/nB:C b/nB:b/nA:B a/nA:A a")
d = clasificar_gramatica("S:C B A/nA B:A/nA:a B c/na B C:a b C/nC:c d")

print(d)"""


"""Pruebas segundo punto"""
"""
a = AutomataPila({'a': [('(', 'Z0', ['(','Z0'], 'a'),
                   ('(', '', ['('], 'a'),
                   (')', '(', [''], 'b')],
             'b': [(')', '(', [''], 'b'),
                   ('$', 'Z0', [''], 'b')]}, ['b'])

b = AutomataPila({'a': [('(', '', ['('], 'a'),
                   (')', '(', [''], 'b')],
             'b': [(')', '(', [''], 'b'),
                   ('(', '', ['('], 'a'),
                   ('$', 'Z0', [''], 'b')]}, ['b'])

c = AutomataPila({'a': [('b', '', ['a'], 'c'),
                        ('a', '', ['a', 'a'], 'a')],
                  'c': [('c', 'a', [''], 'c'),
                        ('$', 'Z0', [''], 'c')]}, ['c'])

d = AutomataPila({'a': [('a', '', ['a'], 'b'),
                   ('b', '', ['b'], 'c')],
             'b': [('a', '', ['a'], 'b'),
                   ('b', 'a', [''], 'b'),
                   ('b', 'Z0', ['b','Z0'], 'c'),
                   ('$', 'Z0', [''], 'b')],
             'c': [('b', '', ['b'], 'c'),
                   ('a', 'b', [''], 'c'),
                   ('a', 'Z0', ['a', 'Z0'], 'b'),
                   ('$', 'Z0', [''], 'c')]}, ['b','c'])

e = AutomataPila({'a': [('a', '', ['a'], 'a'),
                        ('b', '', ['b'], 'b'),
                        ('c', 'a', [''], 'c'),
                        ('$', 'Z0', [''], 'a')],
                  'b': [('b', '', ['b'], 'b'),
                        ('c', 'a', [''], 'c'),
                        ('c', 'b', [''], 'c')],
                  'c': [('c', 'a', [''], 'c'),
                        ('c', 'b', [''], 'c'),
                        ('$', 'Z0', [''], 'c')]}, ['a','c'])

print (e.validar_cadena('aaabbbcccccc$'))"""
